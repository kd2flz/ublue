#!/bin/bash
## Install a base set of bits to mimic the Ubuntu experience
## Intended for non-immutable systems
set -eu

[ "$UID" -eq 0 ] || { echo "This script must be run as root."; exit 1;} # Need to figure out how to pkexec so we only ask for the password once.

BITS=./bits
source $BITS/common

echo "Installing Flatpak(s)..."
flatpak_install_remote flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak_install flathub applications.list

read -n 1 -p "Do you wish to use Flathub beta? (Y/n) " beta
echo ""
case $beta in
    [Yy]* ) 
        flatpak_install_remote flathub-beta https://flathub.org/beta-repo/flathub-beta.flatpakrepo
        flatpak_install flathub-beta applications-beta.list
esac

exit 0;

